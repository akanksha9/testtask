//User Service
app.service('userService', function($http, $log, $location) {
	var service = {}
	var protocol = $location.protocol();
	var host = protocol + '://' + $location.host();
	var port = $location.port();
	var sublink = "users";
	var FullLink = host + ':' + port + '/' + sublink;
 
	service.register = function(data) {
       return $http.post(FullLink + '/register', data)
	}
	service.login = function(data) {
       return $http.post(FullLink + '/login', data)
	}

	return service;

})

//END