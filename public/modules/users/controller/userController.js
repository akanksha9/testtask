//Registering Controller
app.controller('userController', function(userService, $scope, toastr, $state) {

	//Register Function
	$scope.registerUser = (data) => {
		userService.register(data).success((response) => {
			toastr.success("User Registered successfully.");
		}).error((error) => {
			toastr.error(error);
		})

	};

	//Login Function
	$scope.submitLogin = (loginData) => {
		userService.login(loginData).success((response) => {
			$state.go('dashboard');
		}).error((error) => {
             toastr.error("Incorrect Username or Password.");
		})
	}
});

//END