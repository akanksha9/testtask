//Module Initialization and Routing
var app = angular.module('app',['ui.router','toastr']);
app.config(function($stateProvider,$urlRouterProvider){
   $urlRouterProvider.otherwise('/');
	$stateProvider.state('login', {
		url: '/',
		templateUrl : 'modules/users/view/login.html',
		controller:'userController'
	})
	$stateProvider.state('register',{
        url:'/register',
        templateUrl:'modules/users/view/register.html',
		controller:'userController'
	})
	$stateProvider.state('dashboard',{
        url:'/dashboard',
        templateUrl:'modules/dashboard/view/dashboard.html',
		controller:'dashboardController'
	})
});
//END
