//Routes
module.exports = function(app, express) {
    var passport = require('passport');
	var router = express.Router();
	var userController = require('app/controller/userController');

	router.post('/register', userController.register);
	router.post('/login', passport.authenticate('local', {
		session: true
	}), userController.login);
	
	app.use('/users', router);
};
//END