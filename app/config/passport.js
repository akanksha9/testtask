
//Passport Authentication

module.exports = function(app, express, passport) {
	var userModel = require('app/model/userModel')
	var LocalStrategy = require('passport-local').Strategy;
	var jsmd5 = require('js-md5');

	passport.authenticate('LocalStrategy', {
		session: true
	})
	passport.use('local', new LocalStrategy(
		function(username, password, done) {
            var newPass = jsmd5(password);
            userModel.findOne({
				"email": username,
				"password": newPass,
				"isDeleted": false
			}, function(err, user) {
				if (err) {
					return done(err);
				} else {
					if (user == null) {
						return done(null, false);
					} else {
						return done(null, {
							user: user
						})
					}
				}
			});
		}
	));


    passport.serializeUser(function(userObj, done) {
		done(null, userObj);
	});

	passport.deserializeUser(function(userObj, done) {
		done(null, userObj);
	});
}

//END