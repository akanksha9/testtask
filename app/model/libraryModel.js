var mongoose = require("mongoose");
var Schema = require("mongoose").Schema;

var librarySchema = Schema({
    libraryName:{type:String},
    email:{type:String},
    password:{type:String},
    isDeleted:{type:Boolean,default:false}
})


module.exports = mongoose.model('library', librarySchema);