var mongoose = require("mongoose");
var Schema = require("mongoose").Schema;

var userSchema = Schema({
	"library_id":{type:Schema.Types.ObjectId,ref:"library"},
    "username":{type:String},
    "email":{type:String},
    "password":{type:String},
    "isDeleted":{type:Boolean,default:false}
})


module.exports = mongoose.model('user', userSchema);