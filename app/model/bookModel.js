var mongoose = require("mongoose");
var Schema = require("mongoose").Schema;

var bookSchema = Schema({
   "user_id":{type:Schema.Types.ObjectId,ref:"user"},
   "title":{type:String},
   "description":{type:String},
   "price":{type:Number},
   "isDeleted":{type:Boolean,default:false}
})


module.exports = mongoose.model('book', bookSchema);