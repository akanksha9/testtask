var userService = require('app/services/userService');


//*************************************************************
//Register User
//*************************************************************
exports.register = function(req, res) {
	try {
		var obj = req.body;
		userService.register(obj).then(function(userData) {
			return res.status(200).send(userData);
		}).catch(function(err) {
			return res.status(400).send(err);
		})
	} catch (e) {
		return res.status(400).send(e);
	}
};

//END

//*************************************************************
//Login and maintaining session
//*************************************************************
exports.login = function(req, res) {
	try {
		req.session.user = res.req.user;
		if (res.req.user) {
			var outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': 'login successfully',
				'data': res.req.user

			}
			return res.send(outputJSON);
		}
	} catch (e) {
		return res.status(400).send(e);
	}
};

//END