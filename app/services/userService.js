var userModel = require('app/model/userModel');
var Q = require('q');
var md5 = require('js-md5');

function register(payload) {
	var deferred = Q.defer();
	var password = md5(payload.password);
	var obj = {
		"email": payload.email,
		"password": password,
		"username": payload.username
	};

	userModel.find({
		email: payload.email
	}, (err, data)=> {
		if (data && data.length) {
			return deferred.reject("Email already exist");
		} else {
			userModel.create(obj, (err, response)=>{
				if (err) {
					return deferred.reject(err);
				} else {
					return deferred.resolve(response);
				}

			})
		}
	});

	return deferred.promise;
}

var service = {};
service.register = register;
module.exports = service;