require('rootpath')();
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();


//Passport and Session Initialization
var passport = require('passport');
var session = require('express-session');
app.use(passport.initialize());
app.use(passport.session());
app.use(session({
  secret: 'keyboard-cat',
  cookie: {
    maxAge: 26280000000000
  },
  proxy: true,
  resave: true,
  saveUninitialized: true
}));


//Mongo Db Connection with mLabs
var mongoose = require('mongoose');
mongoose.connect('mongodb://akanksha:123456@ds127888.mlab.com:27888/library3');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Routes
var index = require('routes/index');
app.use('/',index);
require('routes/users')(app,express);
require('app/config/passport')(app,express,passport)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
